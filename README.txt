The Engage project is a collection of modules that can be used individually or
together to build out advocacy and engagement functionality within your Drupal
website. For example creating campaigns that track signatories and send out
targeted emails to key decision makers. It is worthwhile noting that this is
being developed in Canada with a focus on targeting Canadian decision makers at
all levels of government, but the goal will be to make it generic enough and
provide integration points so that services linked to other Governments can be
easily integrated.

Services Engage will integrate with
-----------------------------------
opennorth.ca - Canada (not quite finished yet)

Disclaimer
----------
Currently the project is in its infancy and components are being developed and
added. It is not recommended to use the project for live websites until the
7.x-1.0 is released.

What is in it so far
--------------------
1. More or less ready for production

Engage Progress (engage_progress) module:Engage Progress is a simple field
formatter for an integer field that uses the integer value as a "goal" for a
campaign and displays a progress bar as a percentage of how close the campaign
is to achieving that goal. There is API integration for providing the counter
total in that percentage calculation. Out of the box this comes with three
methods for providing a total count:

  - An integer field : Uses the value from another attached integer field as a
    static counter. You would manually update this field to change the progress
    bar.
  - A registration field: Uses the total number of registrations associated with
    an attached Entity Registration registration field as the counter. As an
    example you would attach a registration field to a campaign content type
    for allowing people to "sign" your campaign. The counter total is derived
    from total registrations associated with a particular campaign node.
  - A registration field with integer as a threshold: A combo of the two above
    where the integer is used as a static value added to the registration field
    counter.

2. Other cruft
-----------

FYI: If you clone the repository, the repo currently contains chunks of code
that WILL eventually be brought in as components of Engage.

Maintainers:
-----------
Jesse Payne (acrazyanimal)
Dylan Shields (dshilds)
