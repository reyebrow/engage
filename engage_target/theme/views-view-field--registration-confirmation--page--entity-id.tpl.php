<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php

$output = $field->view->result[0]->registration_entity_id;
global $base_url;
$alias = drupal_get_path_alias($path = 'node/' . $output, $path_language = NULL);
$url = $base_url . '/' . $alias;
$node = node_load($nid = $output, $vid = NULL, $reset = FALSE);
?>
<script>
  function fbShare(url, title, descr, image, winWidth, winHeight) {
    var winTop = (screen.height / 2) - (winHeight / 2);
    var winLeft = (screen.width / 2) - (winWidth / 2);
    window.open('http://www.facebook.com/sharer.php?s=100&p[title]=' + title + '&p[summary]=' + descr + '&p[url]=' + url + '&p[images][0]=' + image, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
  }
</script>
<script src="//platform.twitter.com/widgets.js" type="text/javascript"></script>
<p>We appreciate your support.</p>
<p>Would you like to spread the word to others on
<a href="javascript:fbShare('<?php print $url; ?>', 'Fb Share', 'Facebook share', '', 520, 550)">Facebook</a> or
<a href="https://twitter.com/intent/tweet?url=<?php echo $url; ?>&text=I'm supporting">Twitter</a>?
</p>
<img src="<?php print image_style_url('medium', $node->field_highlighted_image['und'][0]['uri']); ?>" style="float: right; margin-bottom: 10px;" />
