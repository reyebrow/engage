<?php
/**
 * @file
 * Hooks and API available for extending engage_progress.
 */

/**
 * Allow modules to extend the engage_progress module by providing source
 * methods for calculating the current progress for the engage_progress_bar.
 *
 * @return
 *   An associative array of counter sources provided by the implementing module
 *   and indexed by the source type. The array contains the following elements:
 *     - label: The label to display for this method in the formatter settings
 *       option dropdown for 'counter source'.
 *     - settings: An array of additonal formatter settings used by the source
 *       whose keys are the settings form element keys and values are the
 *       default values.
 *     - function: The function to call to retrieve the count for the configured
 *       field using the respective source.
 *
 * @see engage_progress_engage_progress_info().
 * @see _example_counter_random().
 */
function hook_engage_progress_info() {
  return array(
    'random' => array(
      'label' => t('An example random method'),
      'function' => '_example_counter_random',
      'settings' => array('random_start' => 0),
    ),
  );
}

/**
 * Allow modules to override the engage_progress counter sources.
 *
 * @param $info
 *   An associative array of counter sources provided by the implementing module
 *   and indexed by the source type.
 *
 * @see engage_progress_engage_progress_info()
 */
function hook_engage_progress_info_alter(&$info) {
  // We don't want to let anyone use the fixed option.
  unset($info['fixed']);
}

/**
 * Allow modules implementing hook_engage_progress_info to provides additional
 * formatter settings form elements for the engage_progress formatter types
 * they implement.
 *
 * @param $field
 *   The field structure.
 * @param $instance
 *   The field instance settings.
 * @param $view_mode
 *   The view_mode for which the settings are being configured.
 * @param $form
 *   The field formatter form.
 * @param $form_state
 *   The current state of the field formatter form.
 *
 * @see engage_progress_field_formatter_settings_form().
 */
function hook_engage_progress_formatter_settings_form($field, $instance, $view_mode, $form, $form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $element = array();
  if ($display['type'] == 'engage_progress_bar') {
    if (empty($form_state['triggering_element']['#engage_progress']) || !($selected_source = drupal_array_get_nested_value($form_state['values'], $form_state['triggering_element']['#parents']))) {
      $selected_source = $settings['engage_progress_settings']['count_source'];
    }

    if ($selected_source == 'random') {
      $element['random_start'] = array(
        '#title' => t('Random start value'),
        '#description' => t('A value to start a random number from.'),
        '#type' => 'text',
        '#default_value' => ($settings['engage_progress_settings']['random_start']) ? $settings['engage_progress_settings']['random_start'] : '',
      );
    }
  }
}

/**
 * Callback function for our fictional example implementation of a random value
 * counter total calculation.
 */
function _example_counter_random($entity_type, $entities, $field, $instances, $langcode, &$items, $displays) {
  foreach ($displays as $id => $display) {
    $settings = $display['settings']['engage_progress_settings'];
    if ('random' == $settings['count_source']) {
      $start = ($settings['random_start']) ? $settings['random_start'] : 0;
      foreach ($items[$id] as $delta => &$item) {
        $item['count'] = rand($start, $item['value']);
      }
    }
  }
}
